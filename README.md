# tgGrid IE7 Polyfill

This is a polyfill to allow the tgGrid to work in IE7

## Getting Started
Download the [production version][min] or the [development version][max].

[min]: https://raw.github.com/tgui/tgGridIE7Polyfill/master/dist/tgGridIE7Polyfill.min.js
[max]: https://raw.github.com/tgui/tgGridIE7Polyfill/master/dist/tgGridIE7Polyfill.js

In your web page:

```html
<script src="jquery.js"></script>
<script src="dist/tgGridIE7Polyfill.min.js"></script>
<script>
jQuery(function($) {
  $.tgGridIE7Polyfill(); // "awesome"
});
</script>
```

## Documentation
_(Coming soon)_

## Examples
_(Coming soon)_

## Release History
_(Nothing yet)_
